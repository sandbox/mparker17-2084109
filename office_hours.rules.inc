<?php
/**
 * @file
 * Adds rules integration to the office hours module.
 *
 * Designed for use with office_hours-7.x-1.3.
 */

/**
 * Implements hook_rules_data_info().
 */
function office_hours_rules_data_info() {
  $data_types = array();

  // Even though the office_hours field widget *appears* to present a single
  // field-collection that prompts a user to enter opening hours an entire week
  // (the widget doesn't have an "Add another item" button and the UI for the
  // most part treats it as a single thing), the office_hours field widget
  // actually stores at least 7 field values at once.
  //
  // Each field value consists of an opening time, a closing time and a day-of-
  // the-week (for the purposes of brevity, I will refer to this as a 3-tuple).
  //
  // The office_hours module doesn't provide a widget to add a single 3-tuple.
  //
  // For these reasons, I appear to need to name my rules data type as
  // "list<office_hours>". If I don't do it this way, rules asks me to set one
  // 3-tuple at a time.
  $data_types['list<office_hours>'] = array(
    'label' => t('Weekly office hours'),

    // A UI class is used to present the user with a widget when they need to
    // enter an office_hours field value.
    'ui class' => 'OfficeHoursWeeklyRulesDataUI',

    // We need a wrapper class so we can set multiple field data at once.
    'wrap' => TRUE,
    'wrapper class' => 'OfficeHoursWeeklyRulesDataWrapper',
  );

  return $data_types;
}

/**
 * Provides a UI allowing users to enter a week's worth of office_hours data at
 * one time.
 */
class OfficeHoursWeeklyRulesDataUI extends RulesDataUI implements RulesDataDirectInputFormInterface {

  /**
   * @inheritdoc
   */
  public static function getDefaultMode() {
    // There's no documentation on what modes are available. But, the UI class
    // for flags, taxonomy terms, textfields, etc. all use input, so I'm
    // guessing that we're supposed to also.
    return 'input';
  }

  /**
   * @inheritdoc
   */
  public static function inputForm($name, $info, $settings, RulesPlugin $element) {
    global $language;

    // Don't know what this section does, but the basic rules text form does it.
    if (!empty($info['options list'])) {
      $element->call('loadBasicInclude');
      $some_options = call_user_func($info['options list'], $element, $name);
    }
    else {
      RulesDataInputEvaluator::attachForm($form, $settings, $info, $element->availableVariables());
    }
    $settings += array($name => isset($info['default value']) ? $info['default value'] : NULL);

    // Display an office_hours widget (i.e.: entry form).
    //
    // There doesn't appear to be a function that just creates a generic
    // office_hours widget with a bunch of settings, so I had to copy-and-paste
    // from office_hours_field_settings_form(). I changed some of the variable
    // names based on what they do so that they make more sense.
    //
    // @TODO: Because I was trying to use Rules and Views Bulk Operations to set
    // the office hours in a bunch of nodes, I had to load in data about the
    // field using hardcoded names. Ideally, I would get this information from
    // the rule somehow, but I don't know how to do this.
    $field_structure = field_info_field('field_shop_working_hours');
    $field_instance = field_info_instance('node', $field_structure['field_name'], 'shop');
    $form_state = NULL;
    $delta = 0;
    $basic_widget_properties = array();
    $form[$name] = office_hours_field_widget_form($form,
      $form_state,
      $field_structure,
      $field_instance,
      $language->language,
      $field_instance['default_value'],
      $delta,
      $basic_widget_properties
    );

    return $form;
  }

  /**
   * @inheritdoc
   */
  public static function render($value) {
    // @TODO: I don't know when this function is called, so, for debugging
    // purposes, I'm just print_r()ing it. Perhaps eventually, I can use
    // something like theme_office_hours_week().
    return (string) print_r($value, TRUE);
  }

}

/**
 * Provides a data wrapper allowing Rules to work with a week's worth of
 * office_hours data at one time.
 */
class OfficeHoursWeeklyRulesDataWrapper extends RulesIdentifiableDataWrapper implements RulesDataWrapperSavableInterface {

  /**
   * @inheritdoc
   */
  public function set($value) {
    if (isset($value)) {
      // Ensure the fields which must be stored as integers are either integers
      // or NULL. Otherwise we'll get an SQL error.
      foreach ($value as $key => &$office_hour) {
        $office_hour['day'] = !empty($office_hour['day']) ? (integer) $office_hour['day'] : NULL;
        $office_hour['starthours'] = !empty($office_hour['starthours']) ? (integer) $office_hour['starthours'] : NULL;
        $office_hour['endhours'] = !empty($office_hour['endhours']) ? (integer) $office_hour['endhours'] : NULL;
      }
    }
    return parent::set($value);
  }

  /**
   * @inheritdoc
   */
  protected function extractIdentifier($data) {
    // There's no documentation about how the identifier that this function
    // returns is going to be used or what it's supposed to look like. I'm
    // assuming it needs to be a string of some sort.
    $answer = '';

    // Summarize the data.
    if (isset($data)) {
      foreach ($data as $office_hour) {
        $answer .= $office_hour['day']
          . '/'
          . $office_hour['daydelta']
          . ':'
          . $office_hour['starthours']
          . '-'
          . $office_hour['endhours']
          . ';';
      }
    }

    // And, since that string is probably too long, return the md5() of it.
    $answer = md5($answer);
    return $answer;
  }

  /**
   * @inheritdoc
   */
  protected function load($name) {
    // There's no documentation about how this function is supposed to work, so
    // I'm just returning what was given to me.
    return !empty($name) ? $name : FALSE;
  }

  /**
   * @inheritdoc
   */
  public function save() {
    // I assume this is supposed to take care of saving office_hours data, but
    // in my experience, it was never called, so I don't know what to write
    // without hard-coding field names, etc.
    //
    // Perhaps something like:
    // foreach ($this->data as $key => $office_hour) {
    //   db_merge('field_data_field_shop_working_hours')
    //     ->key(array(
    //       'entity_type' => 'node',
    //       'bundle' => 'shop',
    //       'entity_id' => 5,
    //       'revision_id' => 5,
    //     ))
    //     ->fields(array(
    //       'deleted' => 0,
    //       'language' => LANGUAGE_NONE,
    //       'delta' => $key,
    //       'field_shop_working_hours_day' => $office_hour['day'],
    //       'field_shop_working_hours_starthours' => $office_hour['starthours'],
    //       'field_shop_working_hours_endhours' => $office_hour['endhours'],
    //     ))
    //     ->execute();
    // }
  }

}
